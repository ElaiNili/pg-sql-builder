# PG SQL Builder

```javascript
new PgSQLBuilder({
    user: user, // string
    host: host, // int
    database: dbName, // string
    password: pass, // string
    port: port, // int
});
```